package ru.tsc.golovina.tm.command.project;

import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-finish-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().finishById(userId, id);
    }

}
