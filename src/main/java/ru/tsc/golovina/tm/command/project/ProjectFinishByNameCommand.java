package ru.tsc.golovina.tm.command.project;

import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-finish-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().finishByName(userId, name);
    }

}
