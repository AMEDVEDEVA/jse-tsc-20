package ru.tsc.golovina.tm.command.auth;

import ru.tsc.golovina.tm.command.AbstractAuthCommand;

public class LogoffCommand extends AbstractAuthCommand {

    @Override
    public String getCommand() {
        return "logoff";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "User logoff from system";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}
