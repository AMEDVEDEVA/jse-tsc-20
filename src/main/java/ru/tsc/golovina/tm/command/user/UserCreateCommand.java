package ru.tsc.golovina.tm.command.user;

import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class UserCreateCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-create";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().create(login, password, email);
    }

}
