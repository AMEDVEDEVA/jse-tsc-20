package ru.tsc.golovina.tm.command;

import ru.tsc.golovina.tm.exception.empty.EmptyNameException;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    protected void showProjectTasks(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(
                project.getUserId(), project.getId()
        );
        if (tasks.size() <= 0) throw new TaskNotFoundException();
        for (Task task : tasks)
            showTask(task);
    }

    protected void showProject(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    protected void showTask(Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
