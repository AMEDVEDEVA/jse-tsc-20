package ru.tsc.golovina.tm.command.task;

import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-start-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().startByIndex(userId, index);
    }

}
