package ru.tsc.golovina.tm.repository;

import ru.tsc.golovina.tm.api.repository.IOwnerRepository;
import ru.tsc.golovina.tm.exception.empty.EmptyIdException;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(String userId, E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()))
                entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        final List<E> list = findAll(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public E findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        for (final E entity : list) {
            if (entity == null) continue;
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public void clear(final String userId) {
        final List<E> list = findAll(userId);
        list.clear();
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> list = findAll(userId);
        list.remove(entity);
    }

    public Integer getSize(final String userId) {
        final List<E> list = findAll(userId);
        return list.size();
    }

}
