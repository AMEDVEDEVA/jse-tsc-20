package ru.tsc.golovina.tm.repository;

import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : list)
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId()))
                listByProject.add(task);
        return listByProject;
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, final String taskId) {
        final Task task = findById(taskId);
        task.setUserId(null);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject)
            list.remove(task);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : list)
            if (name.equals(task.getName())) return task;
        return null;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, String name, Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
