package ru.tsc.golovina.tm.repository;

import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findUserByLogin(final String login) {
        for (User user : list)
            if (login.equals(user.getLogin())) return user;
        return null;
    }

    @Override
    public User findUserByEmail(final String email) {
        for (User user : list)
            if (email.equals(user.getEmail())) return user;
        return null;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(final String email) {
        return findUserByEmail(email) != null;
    }

}
