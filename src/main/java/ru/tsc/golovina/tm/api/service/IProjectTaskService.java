package ru.tsc.golovina.tm.api.service;

import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, String projectId);

    Task bindTaskById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String projectId, String taskId);

    Project removeById(String userId, String projectId);

    Project removeByIndex(String userId, Integer index);

    Project removeByName(String userId, String name);

}
