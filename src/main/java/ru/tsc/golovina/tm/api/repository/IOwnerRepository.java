package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E add(String userId, E entity);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    void clear(String userId);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    void remove(String userId, E entity);

    Integer getSize(String userId);

}
