package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    boolean existsByName(String userId, String name);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    Task findByName(String userId, String name);

    Task removeByName(String userId, String name);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

}