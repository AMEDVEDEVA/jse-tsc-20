package ru.tsc.golovina.tm.api.service;

import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    void clear(String userId);

    void remove(String userId, E entity);

    Integer getSize(String userId);

}
